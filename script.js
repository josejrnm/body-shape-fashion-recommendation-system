const inputForm = document.getElementById('formData')
const URL = 'http://127.0.0.1:8000/img/photos/'
const btn = document.getElementById('btn')
const result = document.getElementById('Result')

btn.addEventListener('click', async function () {
    const data = new FormData(inputForm);
    
    await fetch(URL, {
        method: 'POST',
        body: data
    })
    .then(response => response.text())
    .then(data => {
        result.innerHTML = data.replace(":","\n");
        console.log(data)
    })
    .catch(error => {console.log(error);alert(error)});
}); 