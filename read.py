import os
import cv2 as cv
import numpy as np
import pandas as pd
import torch
from HierarchicalProbabilistic3DHuman.run_predict import run_predict, body_measurement, save_mesh_as_obj

pose_shape_weights = os.path.abspath('./HierarchicalProbabilistic3DHuman/model_files/poseMF_shapeGaussian_net_weights_female.tar')
pose2D_hrnet_weights = os.path.abspath('./HierarchicalProbabilistic3DHuman/model_files/pose_hrnet_w48_384x288.pth')
pose_shape_cfg = None

gpu: int = 0 
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu)
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Enter to data directory (Photos and measurements)
os.chdir("voluntarias")
dataframes = []
for i in range(1,len(os.listdir())-1,1):
        
        getdata = {}
        error = {}
        os.chdir(f"./{i}")
        
        # Siempre que trabaje con archivos debo utilizar un manejador de contextos (with)
        with open("medidas.txt") as mm:
                estatura = 0.0; n = 1
                for line in mm:
                        key, value = line.split()
                        if key == "Nombre":
                                getdata[key] = [None , value, 'Real']
                                error['0'] = [None]*2 + ['Error']
                        elif key == 'Estatura':
                                estatura = float(value)/100.0
                                pass
                        else:
                                getdata[f'medida {n}'] = [None, key, value]
                                error[f'{n}'] = [None]*2 + [key]
                                n += 1
    
        dirc = os.listdir()
        if 'medidas.txt' in dirc:
                dirc.remove('medidas.txt')
                
        for im in dirc:
                ims = cv.imread(im)
                print(ims)        
#                 cv.imwrite("../png.png", ims)
#                 png = cv.imread("../png.png")
                
#                 getdata["Nombre"].append(f'{im.split(".")[0].capitalize()}')

#                 vertices,faces =  run_predict(device=device,
#                         image= np.array(png),
#                         pose_shape_weights_path=pose_shape_weights,
#                         pose_shape_cfg_path=pose_shape_cfg,
#                         pose2D_hrnet_weights_path=pose2D_hrnet_weights,
#                         already_cropped_images= False,
#                         height = estatura
#                         )

#                 medidas = body_measurement(vertices,faces)
                
#                 for j in range(0,4,1):
#                         if j == 3: getdata[f'medida {j + 1}'].append(str(round(medidas[j]/2,2)).replace(".",","))
#                         else: getdata[f'medida {j + 1}'].append(str(round(medidas[j],2)).replace(".",","))
#                 j = 0
#                 for key in error:
#                         if j == 0:
#                                 error[key].append(f'{im.split(".")[0].capitalize()}')
#                         elif j == 4:
#                                 error[key].append(str(round(((medidas[j-1]/2)-float(getdata[f'medida {j}'][2]))/float(getdata[f'medida {j}'][2])*100,2)).replace(".",",")+'%')
#                         else:
#                                 error[key].append(str(round((medidas[j-1]-float(getdata[f'medida {j}'][2]))/float(getdata[f'medida {j}'][2])*100,2)).replace(".",",")+'%')
#                         j += 1
                                
        
        
#         # Create individual dataframes and store in a list
#         sp = {None: [None] * len(getdata['Nombre'])}
#         ps = {False: [None] * len(getdata['Nombre'])}
#         getdata = {**sp,**getdata,**ps,**error}
#         df2 = pd.DataFrame(getdata)
#         dataframes.append(df2)
#         os.chdir("../")
    
# ## Create and export dataframes
# dataframes = pd.concat(dataframes)
# pd.DataFrame.to_csv(dataframes,"./data.csv",sep=';',index=False)