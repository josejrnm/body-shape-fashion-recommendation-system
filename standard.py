import numpy as np
import numpy as np

#una de las cosas que se pueden hacer para encontrar cual es la talla que mas se asemeja
# Para determinar a qué talla de ropa se asemejan más las medidas de un maniquí en 
# comparación con las tablas que proporcionaste, puedes utilizar métricas de similitud 
# o distancia entre vectores. Aquí hay un enfoque simple utilizando la distancia
#  euclidiana, pero puedes experimentar con otras métricas según tus necesidades.


#nota calcular la talla aproximada para las medidas de la persona y luego 
# hacer un promedio entre la talla dada por la persona y la calculada por el modelo


#reloj de arena  - real 
#rectangulo - calculado con modelo 
# M 100, , 
tabla_dict_camisas = {
    # tall  bus  cin cad ---
    "10":  [80,  72,  86],
    "12":  [82,  74,  88],
    "14":  [86,  76,  92],
    "XS":  [90,  78,  94],
    "S" :  [96,  82,  98], # 94,95,96,97,98
    "M" :  [100, 88,  102],
    "L" :  [106, 94,  118],
    "XL":  [110, 98,  120],
    "XXL": [118, 112, 125],
}

tabla_pantalones = np.array([
    # tall cin cad 
    ["6",  72, 84],
    ["8",  76, 90],
    ["10", 80, 100],
    ["12", 82, 102],
    ["14", 86, 106],
    ["16", 90, 109],
    ["18", 94, 112],
])

medidas_maniqui = {
    "busto": 99.63002389754338,
    "cintura": 85.39643488464347,
    "cadera": 107.75602013617316
}

# Medidas del maniquí 1
maniqui_1_medidas = {"busto": 81, "cintura": 73, "cadera": 91, "hombros": 34}

# Medidas del maniquí 2
maniqui_2_medidas = {"busto": 0.8733838754371156, "cintura": 0.7318665355722626,
                     "cadera": 0.8691205005048704, "hombros": 0.47706473367364427}

# Función para encontrar la talla de blusa


def encontrar_talla_bolsa(medidas_maniqui, tabla_medidas):
    # Convertir la columna de busto a números
    busto_tabla = tabla_medidas[:, 2].astype(float)
    # Comparar las medidas de busto del maniquí con las medidas de la tabla
    busto_maniqui = medidas_maniqui["busto"]
    # Buscar la fila en la tabla donde el busto de la blusa es igual o mayor al busto del maniquí
    fila_talla = np.argmax(busto_tabla >= busto_maniqui)
    talla_encontrada = tabla_medidas[fila_talla, 0]


# # Encontrar la talla de blusa para cada maniquí
# talla_maniqui_1 = encontrar_talla_bolsa(maniqui_1_medidas, tabla_camisas)
# talla_maniqui_2 = encontrar_talla_bolsa(maniqui_2_medidas, tabla_camisas)

# print(f"Talla de blusa para maniquí 1: {talla_maniqui_1}")
# print(f"Talla de blusa para maniquí 2: {talla_maniqui_2}")




from scipy.interpolate import interp1d
import numpy as np

# Medidas del maniquí
medidas_maniqui = {
    "busto": 99.63002389754338,
    "cintura": 85.39643488464347,
    "cadera": 107.75602013617316
}

# Tallas disponibles en la tabla y sus equivalentes numéricos
tallas_tabla = ["10", "12", "14", "XS", "S", "M", "L", "XL", "XXL"]
tallas_numericas = np.arange(len(tallas_tabla))

# Medidas correspondientes a las tallas en la tabla
medidas_tabla = {
    "busto": [80, 82, 86, 90, 96, 100, 106, 110, 118],
    "cintura": [72, 74, 76, 78, 82, 88, 94, 98, 112],
    "cadera": [86, 88, 92, 94, 98, 102, 118, 120, 125]
}

# Realizar la interpolación para cada medida
talla_estimada = {}
for medida in medidas_maniqui:
    f_medida = interp1d(medidas_tabla[medida], tallas_numericas, kind='linear')
    talla_numerica_estimada = f_medida(medidas_maniqui[medida])
    # Asegurarse de que el índice esté dentro de los límites
    talla_numerica_estimada = np.clip(talla_numerica_estimada, 0, len(tallas_tabla) - 1)
    talla_estimada[medida] = tallas_tabla[int(talla_numerica_estimada)]

# Imprimir las tallas estimadas
print("Tallas estimadas para el maniquí:")
for medida, talla in talla_estimada.items():
    print(f"{medida}: {talla}")






from scipy.interpolate import interp1d
import numpy as np

# Medidas del maniquí
medidas_maniqui = {
    "busto": 96.56,
    "cintura": 82.86,
    "cadera": 105.28
}

# busto :96.56996886460485
#  cintura: 82.86548342909207
#  cadera : 105.28066869445081

# Tallas disponibles en la tabla y sus equivalentes numéricos
tallas_tabla = ["10", "12", "14", "XS", "S", "M", "L", "XL", "XXL"]
tallas_numericas = np.arange(len(tallas_tabla))

# Medidas correspondientes a las tallas en la tabla
medidas_tabla = {
    "busto": [80, 82, 86, 90, 96, 100, 106, 110, 118],
    "cintura": [72, 74, 76, 78, 82, 88, 94, 98, 112],
    "cadera": [86, 88, 92, 94, 98, 102, 118, 120, 125]
}

# Realizar la interpolación para cada medida
talla_estimada = {}
for medida in medidas_maniqui:
    f_medida = interp1d(medidas_tabla[medida], tallas_numericas, kind='linear')
    talla_numerica_estimada = f_medida(medidas_maniqui[medida])
    # Asegurarse de que el índice esté dentro de los límites
    talla_numerica_estimada = np.clip(talla_numerica_estimada, 0, len(tallas_tabla) - 1)
    talla_estimada[medida] = talla_numerica_estimada

# Imprimir las tallas estimadas
print("Valores numéricos estimados para el maniquí:")
for medida, talla in talla_estimada.items():
    print(f"{medida}: {talla}")










