from fastapi import (FastAPI,
                     UploadFile,
                     File,
                     Form,
                     Query,
                     HTTPException,
                     status)

from fastapi.responses import RedirectResponse, JSONResponse
from fastapi.middleware.cors import CORSMiddleware

from PIL import Image
import io
import numpy as np
import torch
import os
import cv2
from body_type import BodyType

from HierarchicalProbabilistic3DHuman.run_predict import run_predict, body_measurement, save_mesh_as_obj

app = FastAPI()

app.add_middleware(
                    CORSMiddleware,
                    allow_origins='https://ladysstore.co',
                    allow_credentials=True,
                    allow_methods=['GET', 'POST'],
                    allow_headers=['*']
                    )


@app.get('/',include_in_schema=False)
async def root():
    return RedirectResponse(url='/docs')


@app.post("/IA")
async def upload(file: UploadFile = File(...), height: str = Form(...)):
    """
    This function get uploaded file image to process

    Args:
        file (UploadFile): File Uploaded

    Returns:
        str: Image Data

    Raises:
        ErrorBadRequest: 400
    """

        # Simulación tiempo de espera
    # Validate the size of the uploaded file
    if (file.size/10e5) > 5:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="El archivo excede el tamaño máximo")

    # "image/svg+xml"
    if file.content_type in ["image/jpeg", "image/jpg", "image/gif", "image/webp",]:
        image_content = await file.read()

        # Convertir los bytes en una matriz de píxeles usando OpenCV
        nparr = np.frombuffer(image_content, np.uint8)
        img_np = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

        # Convertir la imagen a formato PNG usando OpenCV
        _, img_png = cv2.imencode('.png', img_np)

        # Crear un objeto de imagen PIL a partir del arreglo de bytes PNG
        png = Image.open(io.BytesIO(img_png.tobytes()))

    pose_shape_weights = os.path.abspath('./HierarchicalProbabilistic3DHuman/model_files/poseMF_shapeGaussian_net_weights_female.tar')
    pose2D_hrnet_weights = os.path.abspath('./HierarchicalProbabilistic3DHuman/model_files/pose_hrnet_w48_384x288.pth')
    image_dir = os.path.abspath('./HierarchicalProbabilistic3DHuman/demo')
    pose_shape_cfg = None

    gpu: int = 0
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    vertices, faces = run_predict(device=device,
                                  image=np.array(png),
                                  pose_shape_weights_path=pose_shape_weights,
                                  pose_shape_cfg_path=pose_shape_cfg,
                                  pose2D_hrnet_weights_path=pose2D_hrnet_weights,
                                  already_cropped_images=False,
                                  height=float(height)
                                  )
    # "C:\Users\jsepu\Downloads\prueba.obj"
    # path = "/mnt/c/Users/jsepu/Downloads/prueba.obj"
    # save_mesh_as_obj(path,
    #                  vertices, faces=faces)

    medidas = body_measurement(vertices, faces)
    body_type = BodyType(busto=medidas[0],
                         cintura=medidas[1],
                         cadera=medidas[2])

    print(f"busto :{medidas[0]} \n cintura: {medidas[1]}\n cadera : {medidas[2]}")
    
    if body_type.is_triangulo():
        return "01"
    elif body_type.is_manzana():
        return "02"
    elif body_type.is_rectangulo():
        return "03"
    elif body_type.is_reloj_arena():
        return "04"
    elif body_type.is_triangulo_invertido():
        return "05"
    else:
        return "no esta parametrizado"



    # triangulo_invertido = body_type.is_triangulo_invertido()
    # rectangulo = body_type.is_rectangulo()
    # triangulo = body_type.is_triangulo()
    # manzana = body_type.is_manzana()
    # reloj = body_type.is_reloj_arena()

    # return {
    #         "triangulo invertido": triangulo_invertido,
    #         "rectangulo": rectangulo,
    #         "triangulo": triangulo,
    #         "manzana": manzana,
    #         "reloj": reloj,
    #         "busto" :medidas[0],
    #         "cintura":medidas[1],
    #         "cadera" :medidas[2]
    #         }


@app.post("/manual")
def predict_shape(busto: str= Form(...), cintura: str = Form(...), cadera: str = Form(...)):

    # Verificar los rangos correstos para cada una de las medidas que se estan usando
    
    busto  =float(busto)
    cadera =float(cadera)
    cintura=float(cintura)

    if (busto > 120) or (busto < 75):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="tamanio de medida fuera de los limites")

    if (cadera > 140) or (cadera < 75):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="tamanio de medida fuera de los limites")

    if (cintura > 130) or (cintura < 59):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="tamanio de medida fuera de los limites")

    body_type = BodyType(busto= busto,
                         cintura=cintura,
                         cadera=cadera,
                         )

    if body_type.is_triangulo():
        return "01"
    elif body_type.is_manzana():
        return "02"
    elif body_type.is_rectangulo():
        return "03"
    elif body_type.is_reloj_arena():
        return "04"
    elif body_type.is_triangulo_invertido():
        return "05"
    else:
        return "no esta parametrizado"