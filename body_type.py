
class BodyType():

    def __init__(self, busto: float = None, cintura: float = None, cadera: float = None):
        self.busto = busto
        self.cintura = cintura
        self.cadera = cadera

    def is_triangulo(self):
        #(Hips - Bust) ≥ 3.6" and (Hips - Waist) < 9"
        #(Caderas - Busto) ≥ 9,144 y (Caderas - Cintura) < 22,86
        result = False  
        if ((self.cadera - self.busto) >= 9.14) and ((self.cadera - self.cintura) < 22,86): 
            result = True 
        return result
    
    def is_triangulo_invertido(self):
        #(Bust - hips) ≥ 3.6" and (Bust - waist) < 9"
        #(Busto - caderas) ≥ 9,144 y (Busto - cintura) <  22,86
        result = False 
        if ((self.busto - self.cadera)>=9.144) and ((self.busto - self.cintura) < 22.86):
            result = True 
        return result 
    
    def is_manzana(self): 
       #(Hips - Bust) > 2" and (Hips - Waist) ≥ 7" and (hip / Waist) ≥ 2
       #(caderas - Busto) > 5.08 and (caderas - cintura) >= 17,78 and (caderas /cintura) >= 5.08
        result = False 
        if (( self.cadera - self.busto) > 5.08 and (self.cadera - self.cintura)>= 17.78 and (self.cadera / self.cintura) >= 5.08):
            result = True 
        return result 

    def is_rectangulo(self):
        #(Hips - Bust) < 3.6" and (Bust - Hips) < 3.6" and (Bust - Waist) < 9" and (Hips - Waist) < 10"
        #(Caderas - Busto) < 9.144 y (Busto - Caderas) < 9.144 y (Busto - Cintura) < 22.86 y (Caderas - Cintura) < 25,4.
        result = False 
        if ((self.cadera - self.busto) < 9.14) and ((self.busto - self.cadera) < 9.14) and ((self.busto - self.cintura) < 22.86) and ((self.cadera - self.cintura) < 25.4): 
            result = True 
        return result 


    def is_reloj_arena(self):
        #(Bust - Hips) ≤ 1" and (Hips - Bust) < 3.6" and ((Bust - Waist) ≥ 9" or (Hips - Waist) ≥ 10")
        #(Busto - Caderas) ≤ 1" y (Caderas - Busto) < 3,6" y ((Busto - Cintura) ≥ 9" o (Caderas - Cintura) ≥ 10").
        #(Busto - Caderas) ≤ 2,54 y (Caderas - Busto) < 9,144 y ((Busto - Cintura) ≥ 22,86 o (Caderas - Cintura) ≥ 25,4).
        result  = False 
        if ((self.busto - self.cadera <= 2.54)) and ((self.cadera - self.busto) < 9.14 ) and (((self.busto - self.cintura) > 22.86) or ((self.cadera - self.cintura) >=25.4)): 
            return True
        #1" < (Bust - Hips) < 10" and (Bust - Waist) ≥ 9"
        if ( 2.54 < (self.busto - self.cadera) <10 and (self.busto - self.cintura) >= 22.86): 
            return True 
        #3.6" ≤ (Hips - Bust) < 10" and (Hips - Waist) ≥ 9" and (High hip / waist) < 1.193
        if ( (9.144 <= (self.cadera - self.busto)<25.4) and (self.cadera - self.cintura) >= 22.86 and (self.cadera / self.cintura) < 4.90): 
            return True 

        return result

